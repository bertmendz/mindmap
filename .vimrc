"===============================================================================
"          File:  customization.vimrc
"   Description:  suggestion for a personal configuration file ~/.vimrc
"   VIM Version:  8.0+
"       Applied:  May 9, 2020
"      Revision:  1 Million
"===============================================================================
" GENERAL SETTINGS
"===============================================================================

if empty(glob('~/.vim/autoload/plug.vim'))
	  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
	      \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
    endif

call plug#begin()
Plug 'mattn/emmet-vim'
Plug 'vim-scripts/bash-support.vim'
" https://github.com/thaerkh/vim-workspace
Plug 'thaerkh/vim-workspace'
"https://github.com/prabirshrestha/asyncomplete.vim
Plug 'prabirshrestha/asyncomplete.vim'
" Ansible-vim
Plug 'pearofducks/ansible-vim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
call plug#end()
"-------------------------------------------------------------------------------
"Plug 'AndrewRadev/splitjoin.vim'
"Plug 'SirVer/ultisnips'
"Plug 'ctrlpvim/ctrlp.vim'
""
" Golang development
"Plug 'fatih/vim-go', { 'do': ':GoInstallBinaries' }
""
" Language autocomplete
"" https://wiki.archlinux.org/index.php/Vim/YouCompleteMe#Installation
"Plug 'Valloric/YouCompleteMe'

"-------------------------------------------------------------------------------
" Use Vim settings, rather then Vi settings.
" This must be first, because it changes other options as a side effect.
"-------------------------------------------------------------------------------
set nocompatible
filetype  plugin on
filetype  indent on
syntax    on
" search down into subfolders
" provide tab completion for all search related tasks
"
" Platform specific items:
" - central backup directory (has to be created)
" - default dictionary
" Uncomment your choice.  
if  has("win16") || has("win32")     || has("win64") || 
  \ has("win95") || has("win32unix")
    "
"    runtime mswin.vim
"    set backupdir =$VIM\vimfiles\backupdir
"    set dictionary=$VIM\vimfiles\wordlists/german.list
else
    set backupdir =$HOME/.vim.backupdir
"    set dictionary=$HOME/.vim/wordlists/german.list
endif
"
" Using a backupdir under UNIX/Linux: you may want to include a line similar to
"   find  $HOME/.vim.backupdir -name "*" -type f -mtime +60 -exec rm -f {} \;
" in one of your shell startup files (e.g. $HOME/.profile)
"
"===============================================================================
" aUTO COMPLETION
" The good stuff in documented in |ins-completion|
"
" hIGHLIGHTS
" ^x^n for just this file
" ^x^f for filenames
" ^x^] for tags only
" ^n for anything specified by the complete option
"
" NOW WE CAN
" Use ^n/^p to go back and forth in the suggestion list
set path+=**
" display all matching files when we tab complete
set wildmenu
"===============================================================================

"===============================================================================
set autoindent                  " copy indent from current line
set autoread                    " read open files again when changed outside Vim
set autowrite                   " write a modified buffer on each :next , ...
set backspace=indent,eol,start  " backspacing over everything in insert mode
set backup                      " keep a backup file
set browsedir=current           " which directory to use for the file browser
set complete+=k                 " scan the files given with the 'dictionary' option
set history=50                  " keep 50 lines of command line history
set hlsearch                    " highlight the last used search pattern
set incsearch                   " do incremental searching
set scrolloff=5
set ignorecase
set smartcase
set listchars=tab:>.,eol:\$     " strings to use in 'list' mode
set mouse=a                     " enable the use of the mouse
set nowrap                      " do not wrap lines
set popt=left:8pc,right:3pc     " print options
set ruler                       " show the cursor position all the time
set shiftwidth=2                " number of spaces to use for each step of indent
set smartindent                 " smart autoindenting when starting a new line
set tabstop=4                   " number of spaces that a <Tab> counts for
set visualbell                  " visual bell instead of beeping
set wildignore=*.bak,*.o,*.e,*~ " wildmenu: ignore these extensions
set clipboard=unnamed
"
"===================================================================================
" BUFFERS, WINDOWS
"===================================================================================
" The current directory is the directory of the file in the current window.
if has("autocmd")
  autocmd BufNewFile,BufRead * :lchdir %:p:h
endif
"
"-------------------------------------------------------------------------------
" close window (conflicts with the KDE setting for calling the process manager)
"-------------------------------------------------------------------------------
noremap  <C-Esc>       :close<CR>
inoremap  <C-Esc>  <C-C>:close<CR>
" swap lines up/down
nnoremap <leader>k :m-2<cr>==
nnoremap <leader>j :m+2<cr>==
xnoremap <leader>k :m-2<cr>gv=gv
xnoremap <leader>j :m'>+<cr>gv=gv
" autoclose tags
" https://aonemd.github.io/blog/handy-keymaps-in-vim/
inoremap ( ()<Left>
inoremap { {}<Left>
inoremap [ []<Left>
inoremap " ""<Left>
inoremap < <><Left>
" act like D and C
nnoremap Y y$

"
"-------------------------------------------------------------------------------
" Fast switching between buffers
" The current buffer will be saved before switching to the next one.
" Choose :bprevious or :bnext
"-------------------------------------------------------------------------------
 noremap  <silent> <s-tab>       :if &modifiable && !&readonly && 
     \                      &modified <CR> :write<CR> :endif<CR>:bprevious<CR>
inoremap  <silent> <s-tab>  <C-C>:if &modifiable && !&readonly && 
     \                      &modified <CR> :write<CR> :endif<CR>:bprevious<CR>
"
"-------------------------------------------------------------------------------
" Leave the editor with Ctrl-q (KDE): Write all changed buffers and exit Vim
"-------------------------------------------------------------------------------
nnoremap  <C-q>    :wqall<CR>
"
"-------------------------------------------------------------------------------
" When editing a file, always jump to the last known cursor position.
" Don't do it when the position is invalid or when inside an event handler
" (happens when dropping a file on gvim).
"-------------------------------------------------------------------------------
if has("autocmd")
  autocmd BufReadPost *
        \ if line("'\"") > 0 && line("'\"") <= line("$") |
        \   exe "normal! g`\"" |
        \ endif
endif " has("autocmd")
"
"-------------------------------------------------------------------------------
" copied from vimrc nuc manjaro box
"-------------------------------------------------------------------------------
inoremap <F6> <C-R>=strftime("%a %b %d, %Y %H:%M")<CR><Esc>0i# <Esc>
nnoremap <F6> "=strftime("%c")<CR>P0i
map <F3> :!clear;python3 %<CR>
map <F5> :!clear;bash %<CR>
"map <F7> :!clear;perl %<CR>
"turn off search highlight
map <F8> :nohlsearch<CR>
imap jk <Esc>
colorscheme elflord
set termguicolors
"-------------------------------------------------------------------------------
" switch buffers {{{
nmap <C-n> :bnext<CR>
nmap <C-p> :bprev<CR>
" }}}
" Fri Nov 22, 2019 05:08
" vim powerline }}}
let g:powerline_pycmd="py3"
set laststatus=2
set rtp+=/usr/lib/python3.7/site-packages/powerline/bindings/vim
" }}}
" insert file name {{{
" Sat 08 Jun 2019 10:40:05 PM +03
inoremap <F7> <C-R>=expand("%:t:r")<CR> <Esc>0i" <Esc>
" }}}
" UI Layout {{{
set number              " show line numbers
set linebreak           " autowrap long lines ensure words are not split
" set auto indent, smart indent
set ai
set si
set relativenumber
set showcmd             " show command in bottom bar
set nocursorline        " highlight current line
set wildmenu
set lazyredraw
set showmatch           " higlight matching parenthesis
set fillchars+=vert:┃
" }}}
" Folding {{{
"set foldmethod=indent   " fold based on indent level
set foldmethod=manual
set foldnestmax=10      " max 10 depth
set foldenable          " don't fold files by default on open
nnoremap <space> za     " toggle folds
set foldlevelstart=10   " start with fold level of 1
"au BufWinLeave * mkview
"au BufWinEnter * silent loadview
" }}}
" Leader Shortcuts, splits {{{
let mapleader=","
nnoremap <leader>s :w<CR>
nnoremap <leader>ps :source $MYVIMRC<CR>

" turn off search highlight
nnoremap <leader><space> :nohlsearch<CR>
nnoremap <leader>m :silent make\|redraw!\|cw<CR>
"note vsp will split window vertical
"july 17, 2018 more related to splits
set splitbelow
set splitright
" scroll screen page up/down
nnoremap <C-j> <PageDown>
nnoremap <C-k> <PageUp>
"
" window split navigations
" move left/right splits
nnoremap <F8> <C-W><C-L>
nnoremap <F9> <C-W><C-H>
"go down
nnoremap <C-J> <C-W><C-J>
"go up
nnoremap <C-K> <C-W><C-K>
"
" autosave vim sessions
au VimLeavePre * if v:this_session != '' | exec "mks! " . v:this_session | endif
" }}}

"-------------------------------------------------------------------------------
" autocomplete parenthesis, brackets and braces
"-------------------------------------------------------------------------------
inoremap ( ()<Left>
inoremap [ []<Left>
inoremap { {}<Left>
"
vnoremap ( s()<Esc>P
vnoremap [ s[]<Esc>P
vnoremap { s{}<Esc>P
"
"-------------------------------------------------------------------------------
" autocomplete quotes
"-------------------------------------------------------------------------------
vnoremap  '  s''<Esc>P<Right>
vnoremap  "  s""<Esc>P<Right>
vnoremap  `  s``<Esc>P<Right>
"-------------------------------------------------------------------------------
inoremap	'  '<Esc>:call QuoteInsertionWrapper("'")<CR>a
inoremap	"  "<Esc>:call QuoteInsertionWrapper('"')<CR>a
inoremap	`  `<Esc>:call QuoteInsertionWrapper('`')<CR>a
"
"-------------------------------------------------------------------------------
" function QuoteInsertionWrapper			{{{3
"
" Add a second quote only if the left and the right character are not keyword
" characters and the right character is not the same quote.
"-------------------------------------------------------------------------------
function! QuoteInsertionWrapper (quote)
  let col   = col('.')
  let line  = getline('.')
  if    ( line[col-2] =~ '\k'    )
  \  || ( line[col  ] =~ '\k'    )
  \  || ( line[col-2] =~ a:quote )
  \  || ( line[col  ] =~ a:quote )
    return a:quote
  else
    return a:quote.a:quote."\<Left>"
  endif
endfunction 
"
"===============================================================================
" VARIOUS CONFIGURATIONS
"===============================================================================
" python development
au BufNewFile,BufRead *.py setfiletype python
au Filetype python set tabstop=4 shiftwidth=4 encoding=utf-8 softtabstop=4 textwidth=79 fileformat=unix expandtab autoindent
map <F4> :!clear;python3 %<CR>
"-------------------------------------------------------------------------------
" Enable to copy to clipboard for operations like yank, delete, change and put
" http://stackoverflow.com/questions/20186975/vim-mac-how-to-copy-to-clipboard-without-pbcopy
if has('unnamedplus')
  set clipboard+=unnamed
  set clipboard+=unnamedplus
endif
"-------------------------------------------------------------------------------
"emmet configuration
"
" https://github.com/mattn/emmet-vim
"Enable just for html/css

let g:user_emmet_install_global = 0
autocmd FileType html,css EmmetInstall

"Redefine trigger key
"remap default <C-Y> leader to <C-K>:
"Note that trailing comma still needs to be entered, so the new keymap would be <C-K>,
"<C-K> is selected since K is near comma, and easy to reach
let g:user_emmet_leader_key='<C-K>'
"
" minimal for yaml editing
"autocmd FileType yml yaml setlocal ts=2 sts=2 sw=2 expandtab indentkeys-=0# indentkeys-=<:> foldmethod=indent nofoldenable
au BufRead,BufNewFile */playbooks/*.yml set filetype=yaml.ansible
au BufRead,BufNewFile */playbooks/*.yaml set filetype=yaml.ansible
" vim: set ft=vim :
