alias c=clear
alias cat='less -R'
alias type='type -a'
export PATH="$HOME/bin:$HOME/.emacs.d/bin:$HOME/.local/bin:$PATH"
export EDITOR="emacsclient -t"
export ALTERNATE_EDITOR=""
alias bemax="emacsclient -c -a emacs"
export VISUAL="emacsclient -c -a emacs"
alias grep='grep -i --color=always'
alias gs='git status '
alias ga='git add '
alias gb='git branch '
alias gba='git branch --all '
alias gc='git commit -m'
alias gd='git diff'
alias gco='git checkout '
alias gk='gitk --all&'
alias glo='git log --oneline'
alias got='git '
alias get='git '
alias gitlog="git log --color --graph --pretty=format:'%Cgreen[%Creset%h%Cgreen]%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
alias gittoday="git log --color --graph --pretty=format:'%Cgreen[%Creset%h%Cgreen]%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --since=yesterday"
alias grv='git remote -v'
alias gall='git log --oneline --graph --all'
alias mypy=python3
