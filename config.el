;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Eribert Mendz"
      user-mail-address "erimendz@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
(setq doom-font (font-spec :family "monospace" :size 14))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-one)
;; tramp mode
(setq tramp-default-method "ssh")

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(require 'org)
(setq org-directory (expand-file-name "~/org"))
(setq org-default-notes-file (concat org-directory "/mygtd.org"))
(setq org-agenda-files '("~/org"))
(setq org-agenda-include-diary t)
(setq org-agenda-diary-file "~/org/diary.org")
(auto-save-visited-mode 1)
(global-magit-file-mode 1)
(blink-cursor-mode 1)
(+ansible-yaml-mode 1)
;;(set-cursor-color DarkOrange)

(setq org-todo-keywords
      '(
        (sequence "IDEA(i)" "TODO(t)" "STARTED(s)" "NEXT(n)" "WARNING(w)" "|" "DONE(d)")
        (sequence "|" "CANCELED(c)" "DELEGATED(l)" "SOMEDAY(f)")
        ))

(setq org-todo-keyword-faces
      '(("IDEA" . (:foreground "GoldenRod" :weight bold))
        ("NEXT" . (:foreground "IndianRed1" :weight bold))
        ("STARTED" . (:foreground "OrangeRed" :weight bold))
        ("WARNING" . (:foreground "coral" :weight bold))
        ("CANCELED" . (:foreground "LimeGreen" :weight bold))
        ("DONE" . (:foreground "LimeGreen" :weight bold :underline t))
        ("SOMEDAY" . (:foreground "LimeGreen" :weight bold))
        ))

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)
(setq global-visual-line-mode t)
(defun my/timestamp ()
   (interactive)
   (insert (format-time-string "<%Y-%m-%d %A %H:%M>")))

;; <2020-04-15 Wednesday 22:14>
;; for literate programming
;; http://howardism.org/Technical/Emacs/literate-programming-tutorial.html
(setq org-confirm-babel-evaluate nil
      org-src-fontify-natively t
      org-src-tab-acts-natively t)

;; setup for org-roam per the below link
;;https://www.ianjones.us/2020-05-05-doom-emacs
;;(setq org-roam-directory "~/org/org-roam")
(setq org-roam-directory "~/org")
(setq org-roam-title-sources '((title headline) alias))
(setq org-roam-tag-sources '(prop last-directory))
(setq org-roam-completion-system 'ivy)
(require 'org-roam-protocol)
;; map custom commands to use with org-roam
(after! org-roam
        (map! :leader
            :prefix "n"
            :desc "org-roam" "l" #'org-roam
            :desc "org-roam-insert" "i" #'org-roam-insert
            :desc "org-roam-switch-to-buffer" "b" #'org-roam-switch-to-buffer
            :desc "org-roam-find-file" "f" #'org-roam-find-file
            :desc "org-roam-show-graph" "g" #'org-roam-show-graph
            :desc "org-roam-insert" "i" #'org-roam-insert
            :desc "org-roam-capture" "c" #'org-roam-capture))

(add-hook 'after-init-hook 'org-roam-mode)
;; for org-journal
(use-package org-journal
      :bind
      ("C-c n j" . org-journal-new-entry)
      :custom
      (org-journal-dir "~/org/org-roam/")
      (org-journal-date-prefix "#+TITLE: ")
      (org-journal-file-format "%Y-%m-%d.org")
      (org-journal-date-format "%A, %d %B %Y"))
    (setq org-journal-enable-agenda-integration t)

(use-package deft
      :after org
      :bind
      ("C-c n d" . deft)
      :custom
      (deft-recursive t)
      (deft-use-filter-string-for-filename t)
      (deft-default-extension "org")
      (deft-directory "~/org/"))
      ;;(deft-directory "~/org/org-roam/"))

;; open remote shell and directory with emacs
;; remote connect to LA cloud playground
;; run this with M-x remshell
;; ++++++++++++++++++++++++++++++++++
;; + change public IP as deemed fit +
;; ++++++++++++++++++++++++++++++++++

;;(defun my/remshell ()
;;    (interactive)
;;    (let ((default-directory "/ssh:cloud_user@13.233.179.123:"))
;;      (ansi-term)))
;;
;;(defun my/remdired ()
;;    (interactive)
;;    (dired "/cloud_user@13.234.111.227:/"))
;;    (let ((default-directory "/ssh:cloud_user@13.234.111.227:")))

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c g k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c g d') to jump to their definition and see how
;; they are implemented.
